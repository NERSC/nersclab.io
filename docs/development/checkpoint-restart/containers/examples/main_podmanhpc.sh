#!/bin/bash

# Slurm directives for job properties
#SBATCH -J test            # Job name
#SBATCH -q debug           # Queue
#SBATCH -N 1               # Number of nodes
#SBATCH -C cpu             # CPU architecture
#SBATCH -t 00:07:00        # Wall clock time
#SBATCH -e %x-%j.err       # Error file
#SBATCH -o %x-%j.out       # Output file
#SBATCH --time-min=00:06:00   # Minimum time allocation
#SBATCH --comment=00:17:00    # Job comment with expected runtime
#SBATCH --signal=SIGTERM@60   # Signal for checkpointing 60 seconds before job ends
#SBATCH --requeue              # Automatically requeue the job if it terminates
#SBATCH --open-mode=append     # Append output to log files

# Load the container image
#SBATCH --image=mtimalsina/geant4_dmtcp:Dec2023   # Container image

# Set up environment and DMTCP coordinator
export DMTCP_COORD_HOST=$(hostname)

# Requeue function to resubmit the job on SIGTERM
function requeue () {
    echo "Got Signal. Going to requeue"
    scontrol requeue ${SLURM_JOB_ID}
}

# Trap SIGTERM signal to trigger requeue function
trap requeue SIGTERM

# Launch the job within the podman-hpc container
podman-hpc run --userns keep-id --rm -it --mpi \
    -e SLURM_JOBID=${SLURM_JOB_ID} \
    -v /cvmfs:/cvmfs \
    -v $(pwd):/podman-hpc \
    -w /podman-hpc \
    mtimalsina/geant4_dmtcp:Dec2023 \
    /bin/bash ./wrapper.sh &

wait