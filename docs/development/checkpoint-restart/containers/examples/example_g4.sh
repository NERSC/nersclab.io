#!/bin/bash

# Source Geant4 data from the container environment
export G4ABLADATA=/cvmfs/geant4.cern.ch/share/data/G4ABLA3.3
export G4LEDATA=/cvmfs/geant4.cern.ch/share/data/G4EMLOW8.5
export G4ENSDFSTATEDATA=/cvmfs/geant4.cern.ch/share/data/G4ENSDFSTATE2.3
export G4INCLDATA=/cvmfs/geant4.cern.ch/share/data/G4INCL1.2
export G4NEUTRONHPDATA=/cvmfs/geant4.cern.ch/share/data/G4NDL4.7
export G4PARTICLEXSDATA=/cvmfs/geant4.cern.ch/share/data/G4PARTICLEXS4.0
export G4PIIDATA=/cvmfs/geant4.cern.ch/share/data/G4PII1.3
export G4SAIDXSDATA=/cvmfs/geant4.cern.ch/share/data/G4SAIDDATA2.0
export G4LEVELGAMMADATA=/cvmfs/geant4.cern.ch/share/data/PhotonEvaporation5.7
export G4RADIOACTIVEDATA=/cvmfs/geant4.cern.ch/share/data/RadioactiveDecay5.6
export G4REALSURFACEDATA=/cvmfs/geant4.cern.ch/share/data/RealSurface2.2

# Set environment variables for the Geant4 benchmark
export G4BENCH_INSTALL=/usr/local
export app=ecal
export NEVENTS=10000000
export log=checkpoint

# Run the Geant4 application with the specified parameters
"$G4BENCH_INSTALL/$app/$app-mt" -n 256 -j "$NEVENTS" -p "PERLMUTTER" -b "$log" >>"$log-n256.log"
: