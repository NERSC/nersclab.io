#!/bin/bash

# Set DMTCP coordinator host and source environment setup
export DMTCP_COORD_HOST=$(hostname)
source my_env_setup.sh

# Function to restart or initiate the job
function restart_job() {
    # Start DMTCP coordinator with a checkpoint interval
    start_coordinator -i 300

    if [[ $(restart_count) == 0 ]]; then
        # Initial job launch
        dmtcp_launch --join-coordinator --interval 300 ./example_g4.sh
        echo "Initial launch successful."
    elif [[ $(restart_count) > 0 ]] && [[ -e dmtcp_restart_script.sh ]]; then
        # Restart the job
        echo "Restarting the job..."
        ./dmtcp_restart_script.sh &
        echo "Restart initiated."
    else
        echo "Failed to restart the job, exiting."; exit
    fi

    # Trap SIGTERM signal to trigger checkpointing
    trap ckpt_dmtcp SIGTERM
}

# Execute the function to restart or start the job
restart_job

# Wait for the job to complete or terminate
wait