#!/bin/bash

# Slurm directives for job properties
#SBATCH -J test            # Job name
#SBATCH -q debug           # Queue
#SBATCH -N 1               # Number of nodes
#SBATCH -C cpu             # CPU architecture
#SBATCH -t 00:07:00        # Wall clock time
#SBATCH -e %x-%j.err       # Error file
#SBATCH -o %x-%j.out       # Output file
#SBATCH --time-min=00:06:00   # Minimum time allocation
#SBATCH --comment=00:15:00    # Job comment with expected runtime
#SBATCH --signal=SIGTERM@60   # Signal for checkpointing 60 seconds before job ends
#SBATCH --requeue              # Automatically requeue the job if it terminates
#SBATCH --open-mode=append     # Append output to log files

# Load required module and container image
#SBATCH --module=cvmfs
#SBATCH --image=mtimalsina/geant4_dmtcp:Dec2023   # Container image with Geant4 and DMTCP setup

# Set up environment and start DMTCP coordinator
export DMTCP_COORD_HOST=$(hostname)
source my_env_setup.sh

# Trap SIGTERM signal to trigger requeue and checkpoint
requeue_job func_trap SIGTERM

# Launch the job within the Shifter container
shifter --module=cvmfs --image=mtimalsina/geant4_dmtcp:Dec2023 /bin/bash ./wrapper.sh &

# Wait for the job to finish or get requeued
wait