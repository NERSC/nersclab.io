#!/bin/bash
# -------------------- Time tracking, signal trapping, and requeue functions ------------------------

# Converts seconds to a human-readable format (days-hours:minutes:seconds)
secs2timestr() {
  ((d=${1}/86400))
  ((h=(${1}%86400)/3600))
  ((m=(${1}%3600)/60))
  ((s=${1}%60))
  printf "%d-%02d:%02d:%02d\n" $d $h $m $s
}

# Converts human-readable time to seconds
timestr2secs() {
  if [[ $1 == *-* ]]; then
    echo $1 | sed 's/-/:/' | awk -F: '{print $1*86400+$2*3600+$3*60+$4}'
  else
    if [[ $1 == *:*:* ]]; then
      echo $1 | awk -F: '{print $3, $2, $1}' | awk '{print $1+60*$2+3600*$3}'
    else
      echo $1 | awk -F: '{print $2+60*$1}'
    fi
  fi
}

# Parses job information, calculates remaining time and requests time for next requeue
parse_job() {
  # Set defaults if not provided
  if [[ -z $ckpt_overhead ]]; then let ckpt_overhead=60; fi
  if [[ -z $max_timelimit ]]; then let max_timelimit=172800; fi

  TOTAL_TIME=$(squeue -h -j $SLURM_JOB_ID -o %k)
  timeAlloc=$(squeue -h -j $SLURM_JOB_ID -o %l)

  # Ensure timeAlloc has at least two fields (hours:minutes)
  fields=$(echo $timeAlloc | awk -F ':' '{print NF}')
  if [ $fields -le 2 ]; then
    timeAlloc="0:$timeAlloc"
  fi

  timeAllocSec=$(timestr2secs $timeAlloc)
  TOTAL_TIME=$(timestr2secs $TOTAL_TIME)

  let remainingTimeSec=TOTAL_TIME-timeAllocSec+ckpt_overhead
  if [ $remainingTimeSec -gt 0 ]; then
    remainingTime=$(secs2timestr $remainingTimeSec)
    scontrol update JobId=$SLURM_JOB_ID Comment=$remainingTime

    jobtime=$(secs2timestr $timeAllocSec)
    if [ $remainingTimeSec -gt $timeAllocSec ]; then
      requestTime=$jobtime
    else
      requestTime=$remainingTime
    fi
    echo "Remaining time: $remainingTime"
    echo "Next timelimit: $requestTime"
  fi
}

# Requeue the job if remaining time is available
requeue_job() {
  parse_job
  if [ -n "$remainingTimeSec" ] && [ $remainingTimeSec -gt 0 ]; then
    func="$1"; shift
    for sig; do
      trap "$func $sig" "$sig"
    done
  else
    echo "No more job requeues, job done!"
  fi
}

# Handles signal and performs checkpointing
func_trap() {
  # Perform checkpoint before requeue
  $ckpt_command
  echo "Signal received. Requeuing job."
  scontrol requeue $SLURM_JOB_ID
  scontrol update JobId=$SLURM_JOB_ID TimeLimit=$requestTime
  echo "Exit status: $?"
}

# Starts the DMTCP coordinator and creates a command wrapper for job communication
start_coordinator() {
  fname=dmtcp_command.$SLURM_JOBID
  h=$(hostname)

  # Check if dmtcp_coordinator is installed
  if ! which dmtcp_coordinator > /dev/null; then
    echo "No dmtcp_coordinator found. Check your DMTCP installation."
    exit 0
  fi

  # Start the coordinator in daemon mode
  dmtcp_coordinator --daemon --exit-on-last -p 0 --port-file $fname $@ 1>/dev/null 2>&1

  # Wait until the coordinator has started and port is available
  while true; do
    if [ -f "$fname" ]; then
      p=$(cat $fname)
      if [ -n "$p" ]; then
        break
      fi
    fi
  done

  export DMTCP_COORD_HOST=$h
  export DMTCP_COORD_PORT=$p

  # Create a DMTCP command wrapper for easy access
  echo "#!/bin/bash
  export PATH=\$PATH
  export DMTCP_COORD_HOST=$h
  export DMTCP_COORD_PORT=$p
  dmtcp_command \$@" > $fname
  chmod a+rx $fname
}

# Waits for the coordinator to complete checkpointing
wait_coord() {
  let sum=0
  ckpt_done=0
  while true; do
    x=($(./dmtcp_command.$SLURM_JOB_ID -s))
    npeers=${x[6]#*=}
    running=${x[7]#*=}
    if [[ $npeers > 0 && $running == no ]]; then
      let sum=sum+1
      sleep 1
    elif [[ $npeers > 0 && $running == yes ]]; then
      ckpt_done=1
      break
    else
      break
    fi
  done

  if [[ $ckpt_done == 1 ]]; then
    echo "Checkpointing completed, overhead = $sum seconds"
  else
    echo "No running job to checkpoint"
  fi
}

# Executes DMTCP checkpointing before requeuing the job
ckpt_dmtcp() {
  ./dmtcp_command.$SLURM_JOB_ID -c
  wait_coord
}
