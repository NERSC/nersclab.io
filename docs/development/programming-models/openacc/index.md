# OpenACC

[OpenACC](https://www.openacc.org) is a directive based programming
model for C, C++ and Fortran primarily focused on offloading work to
accelerators like GPUs. The API consists of compiler directives,
library routines, and environment variables. The [OpenACC
specification](https://www.openacc.org/specification) is a short and
approachable document that is worth reading.

## Using OpenACC on Perlmutter

While several compiler toolchains list support for OpenACC we
recommend users start with NVIDIA's implementation for Perlmutter.

The `PrgEnv-nvidia` module should be used to compile OpenACC code:

| Vendor | PrgEnv          | Language(s)   | OpenACC flag |
|--------|-----------------|---------------|-------------|
| NVIDIA | `PrgEnv-nvidia` | C/C++/Fortran | `-acc`      |

!!! warning "GPU target must be set"
    Either load the `cudatoolkit` and `craype-accel-nvidia80`
    modules or use the the `-acc=gpu` option.

[NVIDIA Compiler
Documentation](https://docs.nvidia.com/hpc-sdk/compilers/index.html)
covers the currently available set of features, examples and a
[Getting started
guide](https://docs.nvidia.com/hpc-sdk/compilers/openacc-gs/index.html).

## References

-  [The OpenACC specificification](https://www.openacc.org/specification)
-  [OpenACC resources](https://www.openacc.org/resources) for guides,
   tutorials, code samples, etc.
-  NERSC OpenACC Training Series, 2020
    - [Part 1: Introduction to OpenACC, April 17,
    2020](https://www.nersc.gov/introduction-to-openacc-part-1-of-3-openacc-training-series-april-17-2020/)
    - [Part 2: OpenACC Data Management, May 28,
    2020](https://www.nersc.gov/openacc-data-management-part-2-of-3-openacc-training-series-may-28-2020/)
    - [Part 3: Loop Optimization with OpenACC, June 23,
    2020](https://www.nersc.gov/loop-optimizations-with-openacc-part-3-of-3-openacc-training-series-june-23-2020/)
