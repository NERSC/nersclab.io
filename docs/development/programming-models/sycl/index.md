# SYCL

SYCL is a cross-platform abstraction layer that enables code for
heterogeneous processors to be written using C++ with the host and
kernel code for an application contained in the same source file.
On NERSC systems, only the Intel 2024.1.0 module supports SYCL.

## Vector Addition Example

`sycl-vecadd-buffer.cpp`

```cpp
--8<-- "docs/development/programming-models/sycl/main.cpp"
```

`Makefile`

```make
--8<-- "docs/development/programming-models/sycl/Makefile"
```

```console
$ module load intel/2024.1.0
$ make
icpx -std=c++17 -fsycl -fsycl-targets=nvptx64-nvidia-cuda -Xsycl-target-backend '--cuda-gpu-arch=sm_80' -o sycl-vecadd-buffer.x sycl-vecadd-buffer.cpp
$ ./sycl-vecadd-buffer.x
sum = 1
```

## oneMKL Example

`sycl-gemm-usm.cpp`

```cpp
--8<-- "docs/development/programming-models/sycl/sycl-gemm-usm.cpp"
```

`Makefile`

```make
--8<-- "docs/development/programming-models/sycl/Makefile.gemm"
```

```console
$ module load intel/2024.1.0
$ export LD_LIBRARY_PATH=/global/common/software/nersc9/onemkl/experimental-2024-06-20/lib:$LD_LIBRARY_PATH
$ make
icpx -std=c++17 -fsycl -fsycl-targets=nvptx64-nvidia-cuda -Xsycl-target-backend '--cuda-gpu-arch=sm_80' -I/global/common/software/nersc9/onemkl/experimental-2024-06-20/include -L/global/common/software/nersc9/onemkl/experimental-2024-06-20/lib -lonemkl -o sycl-gemm-usm.x sycl-gemm-usm.cpp
$ ./sycl-gemm-usm.x
```

## References

* [NERSC, ALCF, Codeplay partnership](https://www.nersc.gov/news-publications/nersc-news/nersc-center-news/2021/nersc-alcf-codeplay-partner-on-sycl-for-next-generation-supercomputers/)
* [DPC++ tutorial](https://github.com/jeffhammond/dpcpp-tutorial)
* [DPC++ Examples from Intel](https://www.intel.com/content/www/us/en/develop/documentation/explore-dpcpp-samples-from-intel/top.html)
* [Free ebook on SYCL programming](https://link.springer.com/book/10.1007/978-1-4842-9691-2)
* [SYCL 2020 Specification](https://www.khronos.org/registry/SYCL/specs/sycl-2020/html/sycl-2020.html)
* [SYCL.tech portal](https://sycl.tech/)
* [Codeplay material](https://github.com/codeplaysoftware/syclacademy/tree/main)
* [oneMKL](https://github.com/oneapi-src/oneMKL/tree/develop)

## Support

* `#sycl` channel in [NERSC Users
  Slack](https://www.nersc.gov/users/NUG/nersc-users-slack/) (login
  required)
* [NERSC Help Desk](https://help.nersc.gov)
