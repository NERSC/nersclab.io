# Compilers

## Overview

Compilers are installed on all NERSC compute systems so that users may compile 
and run their codes. NERSC provides multiple versions of compilers for the 
C, C++, and Fortran languages on each system.

HPE Cray, AMD, NVIDIA, LLVM and GNU compilers are available through
modules on Perlmutter.

!!! note "AMD and LLVM compilers not currently covered"
	Content related to these compilers is under development
	and will be added when ready.

[Compiler wrappers](wrappers.md) provided by the vendor wrap the base
compilers on the machine. These wrappers are the recommended way to invoke
compilers on NERSC machines, for a number of reasons:

1. Compiler wrappers cross-compile for the appropriate compute-node target.
2. Compiler wrappers automatically include the path to MPI header files and
take care of resolving MPI paths during the linking stage of compilation.
3. The command to invoke the compiler is independent of the underlying base 
compiler. This results in fewer changes to your Makefile when switching to
a new base compiler. 

The set of compilers underlying the compiler wrappers are known as 
[base compilers](base.md). The available base compilers vary depending
on the compute system. Base compilers, without wrappers, can also be used to
compile non-MPI codes. 
