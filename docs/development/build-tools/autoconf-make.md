# Build tools

## GNU Autoconf

GNU Autoconf is a tool for producing configure scripts for building,
installing and packaging software on computer systems where a Bourne
shell is available.

The code's `configure` script takes options, which set variables in the 
makefile. The following table illustrates some common options encountered.

| Option           | Explanation                          | Example     |
|------------------|--------------------------------------|-------------|
| `--prefix`         | Directory to install the code into | `--prefix=$CFS/m1234/myapp/` |
| `variable=VALUE`   | Setting environment variable, commonly used for compiler | `F90=ftn` |
| `--enable-FEATURE` | Enable `FEATURE` in the settings   | `--enable-mpi` |
| `--with-PACKAGE`   | Use optional package `PACKAGE`     | `--with-fftw` |
| `--help`           | Lists all options to `configure`   |   |

The `variable=VALUE` option is almost always necessary at NERSC, because
configure scripts usually cannot find the 
[compiler wrappers](../compilers/wrappers.md).

```shell
./configure CC=cc CXX=CC FC=ftn F77=ftn
```

A hypothetical configure line using additional options could look something 
like this:

```shell
./configure --prefix=$CFS/m1234/myapp --enable-mpi --with-fftw-dir=$FFTW_ROOT CC=cc MPICC=cc CFLAGS="-O3 -Wall"
```

## GNU Make

[Make](https://en.wikipedia.org/wiki/Make_(software)) is a common
build automation tool in wide use by Unix like systems. The most widespread
implementation is [GNU Make](https://www.gnu.org/software/make/), which is
the default for both Mac OS X and most Linux distributions.

A typical Makefile looks something like this:

```make
TARGET = test
LIBS =
CC = cc
CFLAGS = -g -Wall -qopenmp

.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)
```

### Editing Makefiles

The basic concept behind Makefiles is that there is a file known as a
*makefile* that controls how the code is compiled into object files and linked.
Within the makefile, the name of the compiler, compiler flags, and other
settings are made. For more information about Makefiles, please see the Software 
Carpentry [tutorial](http://swcarpentry.github.io/make-novice/) on Makefiles.

Many codes have a build system that uses a build tool such as autoconf or
CMake to customize the makefile with the proper settings. Still other codes
depend on the user to directly edit the makefile to add the proper settings by
hand. The file to be edited is usually named `Makefile` or `makefile`.

Sometimes a code will provide pre-populated custom makefiles for common
platforms, e.g., `Makefile.perlmutter`, that can be used directly, either
by renaming the file to `Makefile` or entering the name of the file into
the main makefile. If something like this is the case for your code,
instructions may be available in a file which is often called `README` or 
`INSTALL`.

If the code does not have a custom Makefile for this platform, then you will
need to edit the Makefile. Read the compilation instructions. For some codes,
we edit the file `Makefile`; in others there may be a file called `Make.inc`.
It really depends on the way the developers set it up.

In the appropriate file, there are likely macros (think of these as variables)
set up for the names of the compilers and compiler flags. In addition, there
may be macros for the include directories, link directories, and libraries used 
by the code. You can leave many of these latter macros blank, especially those
pertaining to MPI. 

!!! tip
	MPI codes distributed via make have lines like `CC = mpicc`. In
	most cases it is sufficient to change these lines to `CC = cc`.

You will commonly need to set the compiler to be invoked. Unless the code does
not use MPI and is meant to be run on a login node, use the Cray compiler 
wrappers `cc`, `CC`, and `ftn` as the C, C++, and Fortran compilers, 
respectively. You may want to use optimizations, which are specific to the 
compiler invoked by the compiler wrapper. Please see the 
[pages on compilers](../compilers/base.md) for specifics.

Especially if you are compiling a C or C++ code, you may need to set a macro
that is often named `INCLUDE` or `INCLUDEDIR` to point to directories where 
header files (referenced like `#include <file.h>` within the code) are located.
