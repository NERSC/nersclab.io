# Account Policies

There are a number of policies which apply to NERSC users. These
policies originate from a number of sources, such as DOE regulations
and DOE and NERSC management.

## User Account Ownership, Password, and MFA Policies

A user is given a username (also known as a login name) and creates a
password and Multi-Factor Authentication (MFA) token that enable them 
to access NERSC resources. This username and its associated password 
and MFA may be used by a single individual only.

**Passwords may not be shared, and must be created or changed using
specified rules**. See [NERSC Passwords](passwords.md) for more
information.

Likewise, [MFA tokens](../connect/mfa.md) must be kept secure and may
not be shared with other users.

NERSC will disable a user who shares any one of their passwords with
another person. If a person authenticating to a username is not the
one who is officially registered with NERSC as the owner of that
username, then "sharing" has occurred and all usernames associated
with the owner of the shared username will be disabled.

If a user is disabled due to account sharing, the PI or an authorized
project manager must send a memo to accounts@nersc.gov explaining why
the sharing occurred and assuring that it will not occur again. NERSC
will then determine if the user should be re-enabled.

The computer use policies and security rules that apply to all users
of NERSC resources are listed in the 
[Appropriate Use Policy](https://www.nersc.gov/users/policies/appropriate-use-of-nersc-resources/) 
form.

## Security Incidents

If you think there has been a computer security incident you should
contact NERSC Security as soon as possible at security@nersc.gov.

Please save any evidence of the break-in and include as many details
as possible in your communication with us.

## Account Lifecycle

### Account Creation

An account begins with a request for a new account. Account requests must
be associated with a project.

Once the account request is received, NERSC vets the prospective user. This
process generally takes less than a week, though it could take longer in some
cases.

Following NERSC's vetting, the project PI must approve the user to join the
requested project. After that, the account is ready for the user to set their
new password and set up multi-factor authentication.

### Account Continuation

NERSC accounts are tied to project allocations. For an account to remain
active, it must be associated with an active project. In addition, the user of
the account must agree to the NERSC 
[Appropriate Use Policy](https://www.nersc.gov/users/policies/appropriate-use-of-nersc-resources/)
and
[Code of Conduct](https://www.nersc.gov/users/nersc-code-of-conduct/). 
These documents are periodically updated and users must accept the new terms
to continue using NERSC resources.

## Account Deactivation

There are several ways that an account could become deactivated.

- **Allocation-year discontinuation:** If a PI does not elect to continue a
user in their renewed project in the new allocation year, and that user is not
a member of another active project in the new allocation year, then the user
account will be discontinued at the start of the new allocation year.
- **User-initiated deactivation:** A user who wants to close their account can 
do so in [Iris](https://iris.nersc.gov/) or by emailing NERSC account support 
at accounts@nersc.gov with their request to deactivate their account.
- **PI-initiated project disassociation:** A PI may remove a user from their 
project during the allocation year. If the user is not a member of another 
project, their account would no longer be associated with an active project, 
and the account would become deactivated.
- **Limited-term project membership expiration:** PIs have the option to set 
an expiration date for project membership (e.g., for a summer intern who will
not continue after the internship). A user account for which
access to all projects has ended would become deactivated.
- **Policy Agreement expiration:** Periodically, NERSC may update its
[Appropriate Use Policy](https://www.nersc.gov/users/policies/appropriate-use-of-nersc-resources/)
and [Code of Conduct](https://www.nersc.gov/users/nersc-code-of-conduct/). 
Users who do not attest to the new policies may have their accounts deactivated
after the deadline to accept the new policies passes.
- **Security deactivation:** If a user violates NERSC security policy or for
other security-related reasons, NERSC may deactivate a user account.

### Account Deactivation Process

In the case of **allocation-year discontinuation**, there is a 14-day period
during which the user can still log onto computational systems but cannot
submit jobs. The user account then enters a 60-day "data-access only" state, 
in which the user can log in to NERSC's
[data transfer nodes](../systems/dtn/index.md) and 
[HPSS](../filesystems/archive.md), in order to retrieve data. Following that
period, the account becomes "expired", and after 30 more days, the account is
deactivated and its password and MFA token are deleted.

For **user-initiated deactivations**, **PI-initiated project disassociations**, 
**limited-term project membership expirations**, **policy agreement 
expirations**, and **security deactivations**, the user account is immediately
deactivated (moving to the "expired" state); the password and MFA token(s)
 associated with the account are deleted.

## Acknowledge use of NERSC resources

Please acknowledge NERSC in your publications, for example:

>This research used resources of the National Energy Research
>Scientific Computing Center, which is supported by the Office of
>Science of the U.S. Department of Energy under Contract
>No. DE-AC02-05CH11231.
