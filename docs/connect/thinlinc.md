# ThinLinc

[ThinLinc](https://www.ThinLinc.com/) is a program 
that handles remote X Window System connections and offers several performance 
improvements over traditional X11 forwarding. ThinLinc can greatly improve 
the response time of X Windows and is 
the recommended method of interacting with GUIs and visualization tools running
on NERSC resources. ThinLinc also allows a user to disconnect from a session 
and reconnect to it at a later time while keeping the state of all running
applications inside the session.

NERSC has chosen ThinLinc because we believe it will provide a much 
more scalable and performant solution for our virtual desktop users, 
allowing us to better fit the capacity and needs of our users. Switching 
to ThinLinc will also allow NERSC staff to upgrade the underlying operating system 
and software involved with our remote desktop solution more easily, 
allowing us to be more responsive to security updates.

## Getting Started with ThinLinc

![ThinLinc Desktop Example Image](images/thinlinc_desktop.png){: width="800" }

### Install the ThinLinc Client

To use ThinLinc at NERSC, you will first need to download and install the 
appropriate version of the [ThinLinc Client](https://www.cendio.com/thinlinc/download)
for the operating system on your laptop or workstation.

??? example "macOS Installation Tip"
    Some users may not see the ThinLinc client mounted drive on their Desktop 
    because of their macOS settings. If you don't see the mounted client drive,
    open a finder window, and click on the left side tab that says "ThinLinc Client"
    which will show the UI to move the ThinLinc client to the Applications directory.

    ![ThinLinc Client install from the macOS finder window](images/thinlinc_macos_install.png){: width="512" }

### Configure a NERSC connection

ThinLinc at NERSC is hosted via the SSH protocol, so you must configure your 
connection to NERSC to authenticate via SSH.  This can be done with
an ssh key generated from [sshproxy](../connect/mfa.md#sshproxy).
<!-- or with your Password+OTP.  -->
You will need to regenerate your
ssh key pair with sshproxy once a day, but once your key is generated the 
ThinLinc Client be able to connect.

To set up your first ThinLinc connection, follow the instructions for 
"Authenticate via sshproxy".

<!-- To set up your first ThinLinc connection, select your 
desired authentication method below and follow the instructions. -->

??? example "Authenticate via sshproxy"

    1. Click on "Advanced" button to get all the options
    2. Click on "Options" button to open options window
    3. Click on the "Security" tab at the top of the window
    4. Under "Authentication Method" select "Public Key"
    5. Click "ok" to save your selection
    6. Enter the connection details for ThinLinc
        - Server: `thinlinc.nersc.gov`
        - User: Your NERSC username
        - Key: Path to your ssh key

    ![ThinLinc example logging in with ssh keys](images/thinlinc_sshkey_example.png){: width="512" }

<!-- ??? example "Authenticate with password and OTP"

    1. Click on "Advanced" button to get all the options
    2. Enter the connection details for ThinLinc
        - Server: `thinlinc.nersc.gov`
        - User: Your NERSC username
        - Password: Your Password+OTP

    ![ThinLinc example logging in with ssh keys](images/thinlinc_passwordotp_example.png){: width="512" } -->

### Current Issues

There are a few known user experience issues 
which we are actively working on enabling.
If you find other issues with ThinLinc please open a ticket at [help.nersc.gov](https://help.nersc.gov).

 - Only authentication with sshproxy keys is enabled.
 - Collaboration accounts cannot connect to ThinLinc.

<!-- ### Verify host on first connection

Upon making your first connection to NERSC via the ThinLinc client, you should 
see a message asking to verify the authenticity of the NERSC ThinLinc portal:

The displayed key should match one of NERSC's 
[published key fingerprints for ThinLinc](index.md#ThinLinc).

!!! danger
    If you notice that the key that ThinLinc displays does not match
    the [keys published on the NERSC website](index.md#key-fingerprints),
    please stop and contact us immediately at the
    [NERSC Help Desk](https://help.nersc.gov). -->
