# NCL

NCL (NCAR Command Language) is an interpreted language designed specifically for
scientific data analysis and visualization. NCAR provides
[official documentation regarding NCL](https://www.ncl.ucar.edu/).

## Usage Summary

### Perlmutter

NCL is available on Perlmutter as part of the `climate-utils` module.

To use NCL:

```console
module load climate-utils
```

## Related Documentation

The following is a list of related Web documentation for NCL:

   * [NCL Tools Documentation](http://www.ncl.ucar.edu/Document/Tools/)
   * [NCL Reference Manual](http://www.ncl.ucar.edu/Document/Manuals/Ref_Manual/)
   * [Getting Started using NCL](http://www.ncl.ucar.edu/Document/Manuals/Getting_Started/)
   * [Category List of NCL Applications](http://www.ncl.ucar.edu/Applications/)
