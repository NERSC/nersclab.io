# Q-Chem

[Q-Chem](http://www.q-chem.com/) is a comprehensive ab initio quantum chemistry package for
accurate predictions of
molecular structures, reactivities, and spectra. It provides a wide variety of options for
molecular modeling, including over 200 DFT functionals; excited-state modeling with TDDFT;
wavefunction theory methods such as coupled cluster and perturbation theory; implicit and explicit
solvation modeling; QM/MM simulations; and more.

Users are strongly encouraged to review the [Q-Chem manual](https://manual.q-chem.com/latest/)
for more information on
constructing input files, running jobs, and available features. The
[Q-Chem website](http://www.q-chem.com/) also contains
many useful learning materials, including lab exercises, webinars, and workshops.

Q-Chem also offers a free open-source molecule builder and visualizer, IQmol. For more information
please see the website, https://www.q-chem.com/try/iqmol/.

## Using Q-Chem At NERSC

Q-Chem is available to all NERSC users on Perlmutter via NERSC’s site license; a separate Q-Chem
license is not required for individual users, unless some additional functionality not covered by
the site license (such as [BrianQC](https://www.brianqc.com/)) is required.

Q-Chem is available as a module, and can be loaded using:

```
module load qchem
```

A list of available Q-Chem versions can be obtained by running the command:

```
module avail qchem
```

### Sample Q-Chem Batch Job Script

A simple example of a batch job script for running a Q-Chem job on Perlmutter is found below:

```
#!/bin/bash
#SBATCH --job-name=h2.in
#SBATCH --qos=shared
#SBATCH --account=<account>
#SBATCH --time=24:00:00
#SBATCH -C cpu
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=h2.err
#SBATCH --error=h2.err

module load qchem
export QCLOCALSCR=$QCSCRATCH
export OMP_NUM_THREADS=16
export QC_THREADS=16

###run q-chem
qchem -save -nt 16 h2.in h2.out h220240503112508 > h2.err

rm -rf $QCSCRATCH/h220240503112508
###

echo "Finished running "
echo "================="
echo " "
echo "Job ended at :"
date
```
<!--
The section below does not function correctly. The qchem group needs to change permissions on
the script to make it available to all users. Since the rest of the information is good, and
helpful to users, I'm going to leave it as a comment until fixed -- EPalmer.
-->
<!--
### Python Script For Generating sbatch Job Scripts

There is also a Python script available to NERSC users, which can be used to automatically submit
Q-Chem jobs on Perlmutter. This script can be found on Perlmutter under,

```
/global/cfs/cdirs/qchem/shellscripts/submit_qchem_nersc.py
```

The script can be run using the following:

```
submit_qchem_nersc -p 16 -q shared --acc=<account> job.in
```
-->

Users can copy the script to their own directory and specify a default account to use by modifying
the script, if desired.

### Additional Tips & Suggestions

Choice of `QCSCRATCH` directory

The scratch directory that Q-Chem uses is defined by the `QCSCRATCH` environment variable. For
optimal performance, is important to choose a directory which has fast I/O to the node on which
you are running the calculation.  On Perlmutter, it is generally best to use Perlmutter Scratch,
outlined in [NERSC’s documentation](../../filesystems/perlmutter-scratch.md). This can be done
in the input sbatch script by adding the line:

```
export QCSCRATCH=$PSCRATCH
```

Please note that files are Perlmutter Scratch are purged 8 weeks after creation. If retaining the
scratch files is desirable, one should copy the files over to a more permanent location after the
calculation is complete.

## General Running Instructions

General instructions for running Q-Chem jobs can be found in the general usage section of the
Q-Chem manual under
[General Usage](https://manual.q-chem.com/latest/ssect_general-usage.html). Additional instructions
for Q-Chem input file setup are also available in the Q-Chem manual
[Overview](https://manual.q-chem.com/latest/Ch3.S2.SS1.html).
Further details on running specific job types, as well as sample jobs, can also be found in the Q-Chem manual.

## Support

Q-Chem users are encouraged to post on the [Q-Chem Talk forum](https://talk.q-chem.com/) with any questions. They can
also reach out to the Q-Chem support team via email at [support@q-chem.com](mailto:support@q-chem.com).
