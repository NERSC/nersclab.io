# Spack v0.22

The spack v0.22 stack is built using the Spack release [v0.22](https://github.com/spack/spack/releases/tag/v0.22.0). 
The software stack is built using the *gcc* compiler. You can access this software stack by running:

```
module load spack/0.22
```

Once you load the module, you will get a message banner, its worth noting that one needs to change to a spack environment
to access software.

```shell
elvis@perlmutter> module load spack/0.22
    _______________________________________________________________________________________________________
     Welcome to Spack!

     In order to access the production stack, you will need to load a spack environment. Here are some tips to get started:


     'spack env list' - List all Spack environments
     'spack env activate gcc' - Activate the "gcc" Spack environment
     'spack env status' - Display the active Spack environment
     'spack load amrex' - Load the "amrex" Spack package into your user environment

     For additional support, please refer to the following references:

       NERSC E4S Documentation: https://docs.nersc.gov/applications/e4s/
       E4S Documentation: https://e4s.readthedocs.io
       Spack Documentation: https://spack.readthedocs.io/en/latest/
       Spack Slack: https://spackpm.slack.com

    ______________________________________________________________________________________________________
```

To see available spack environments you can run the following

```shell
elvis@perlmutter> spack env list
==> 2 environments
    cuda  gcc
```

If you want to activate a spack environment you can run the following

```shell
elvis@perlmutter> spack env activate cuda
elvis@perlmutter> spack env st
==> In environment cuda
```

## Breakdown of Installed Specs

Shown below is a breakdown of installed specs by environment.

| Spack Environments | Compiler     | Root Specs | Depedencies Specs | Total Packages | Required Modules                                    |
|--------------------|--------------|------------|-------------------|----------------|-----------------------------------------------------|
| `gcc`              | `gcc@12.3.0` | 63         | 131               | 193            | `PrgEnv-gnu`, `gcc-native/12.3`                     |
| `cuda`             | `gcc@12.3.0` | 12         | 35                | 56             | `PrgEnv-gnu`, `gcc-native/12.3`, `cudatoolkit/12.2` |

Shown below are the root specs installed for each environment

??? "Spack Environment - gcc"

    ```shell
    elvis@perlmutter> spack find -rf
    ==> In environment gcc
    ==> 63 root specs
    -- no arch / gcc@12.3.0 -----------------------------------------
    [+] adios2%gcc@12.3.0
    [+] amrex%gcc@12.3.0
    [+] autoconf%gcc@12.3.0
    [+] automake%gcc@12.3.0
    [+] boost%gcc@12.3.0
    [+] butterflypack%gcc@12.3.0
    [+] caliper%gcc@12.3.0
    [+] cdo%gcc@12.3.0
    [+] chapel%gcc@12.3.0
    [+] cmake%gcc@12.3.0
    [+] datatransferkit%gcc@12.3.0
    [+] fortrilinos%gcc@12.3.0
    [+] gasnet%gcc@12.3.0
    [+] gawk%gcc@12.3.0
    [e] git%gcc@12.3.0
    [+] gmake%gcc@12.3.0
    [+] gromacs%gcc@12.3.0
    [+] gsl%gcc@12.3.0
    [+] h5bench%gcc@12.3.0
    [+] hdf5%gcc@12.3.0
    [+] heffte%gcc@12.3.0 +fftw
    [+] hpctoolkit%gcc@12.3.0
    [+] hpx%gcc@12.3.0  max_cpu_count=512 networking=mpi
    [+] hypre%gcc@12.3.0
    [+] kokkos%gcc@12.3.0 +openmp
    [+] kokkos-kernels%gcc@12.3.0 +openmp
    [+] libcint%gcc@12.3.0
    [+] libint%gcc@12.3.0  tune=molgw-lmax-7
    [+] libxc%gcc@12.3.0
    [+] likwid%gcc@12.3.0
    [+] mercury%gcc@12.3.0
    [+] metis%gcc@12.3.0
    [+] mfem%gcc@12.3.0
    [+] nano%gcc@12.3.0
    [+] nccmp%gcc@12.3.0
    [+] ncl%gcc@12.3.0
    [+] nco%gcc@12.3.0
    [+] nco%gcc@12.3.0
    [+] ncview%gcc@12.3.0
    [+] netcdf-c%gcc@12.3.0 ~mpi
    [+] netcdf-fortran%gcc@12.3.0
    [+] netlib-scalapack%gcc@12.3.0
    [+] openpmd-api%gcc@12.3.0
    [+] papi%gcc@12.3.0
    [+] parmetis%gcc@12.3.0
    [+] pdt%gcc@12.3.0
    [+] petsc%gcc@12.3.0
    [+] phist%gcc@12.3.0
    [+] plasma%gcc@12.3.0
    [+] plumed%gcc@12.3.0
    [+] py-h5py%gcc@12.3.0
    [+] py-petsc4py%gcc@12.3.0
    [+] qthreads%gcc@12.3.0  scheduler=distrib
    [+] slate%gcc@12.3.0 ~cuda
    [+] slepc%gcc@12.3.0
    [+] strumpack%gcc@12.3.0 ~slate
    [+] sundials%gcc@12.3.0
    [+] superlu%gcc@12.3.0
    [+] superlu-dist%gcc@12.3.0
    [+] swig%gcc@12.3.0
    [+] tau%gcc@12.3.0 +mpi+python
    [+] trilinos%gcc@12.3.0 +amesos+amesos2+anasazi+aztec+belos+boost+epetra+epetraext+ifpack+ifpack2+intrepid+intrepid2+isorropia+kokkos+minitensor+ml+muelu+nox+phalanx+piro+rol+rythmos+sacado+shards+shylu+stk+stokhos+stratimikos+superlu-dist+teko+tempus+tpetra+trilinoscouplings+zoltan+zoltan2 gotype=long_long
    [+] upcxx%gcc@12.3.0
    
    ==> 193 installed packages (not shown)
    ```

??? "Spack Environment - cuda"

    ```shell
    elvis@perlmutter> spack find -rf
    ==> In environment cuda
    ==> 12 root specs
    -- no arch / gcc@12.3.0 -----------------------------------------
    [+] amrex%gcc@12.3.0 +cuda cuda_arch=80   [+] kokkos%gcc@12.3.0 +cuda+wrapper cuda_arch=80  [+] petsc%gcc@12.3.0 +cuda cuda_arch=80  [+] strumpack%gcc@12.3.0 +cuda~slate cuda_arch=80
    [+] heffte%gcc@12.3.0 +cuda cuda_arch=80  [+] kokkos-kernels%gcc@12.3.0 +cuda cuda_arch=80  [+] slate%gcc@12.3.0 +cuda cuda_arch=80  [+] sundials%gcc@12.3.0 +cuda cuda_arch=80
    [+] hypre%gcc@12.3.0 +cuda cuda_arch=80   [+] magma%gcc@12.3.0 +cuda cuda_arch=80           [+] slepc%gcc@12.3.0 +cuda cuda_arch=80  [+] superlu-dist%gcc@12.3.0 +cuda cuda_arch=80
    
    ==> 56 installed packages (not shown)
    ```
