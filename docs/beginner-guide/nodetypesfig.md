# Node Types at NERSC

Three main boxes arranged vertically and labeled "Login Node," "CPU-only Nodes," and "GPU 
Nodes." An arrow with the text "Login to Perlmutter" points at the "Login Node" box. 
Inside the "Login Node" box is a diagram showing how the CPUs and memory are connected for 
these kinds of nodes. There are two CPUs, each connected to its own separate memory. A box 
beside them says: "Only for logging in and basic tasks like submitting jobs to the job 
scheduler (not for running computation!)". Within the "CPU-only Nodes" main box there is a 
copy of the same diagram of two CPUs and their associated memory. A box beside the diagram 
describes these nodes: "3072 Nodes on Perlmutter, Architecture: 2 AMD EPYC 7763 CPUs per 
node, Memory per node: 512 GB". Within the "GPU Nodes" main box, there is a diagram 
containing one CPU connected to two memory units, as well as connected to 4 GPUs. Each GPU 
is connected to each other GPU, though only two of the GPUs are directly connected to the 
CPU. One GPU has an arrow pointing to a rectangle labeled "extra memory". The rectangle 
has the caption "some large memory GPU nodes available". A box beside this diagram reads: 
"1794 Nodes (includes 256 large-memory GPU Nodes), Architecture: 1 AMD EPYC 7763 CPUs and 
4 NVIDIA A100 GPUs, Memory per Node: 256 GB (40 GB / 80 GB per large-memory GPU)."
