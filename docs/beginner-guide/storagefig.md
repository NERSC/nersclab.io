# Storage Systems at NERSC

In the center, three vertically stacked boxes are labeled "Scratch" at the top, "Community 
File System" in the middle, and "High Performance Storage System (HPSS)" on the bottom. An 
arrow with the text "increasing data rates" is to the left of the stack, pointing up. An 
arrow with the text "increasing storage space" is on the right side of the stack, pointing 
down. Within the top box labelled "Scratch", there is text reading "Data speed: 5 TB/s, 
Space per user: 20 TB." Within the box labelled "Community File System" there is text 
reading "Data speed: 100 GB/s, Space per user: varies by allocation."  Within the box 
labelled "High Performance Storage System (HPSS)" there is text reading "Data speed: 50 
GB/s, Space per user: varies by allocation." Beneath the vertically stacked boxes and 
their corresponding arrows are two horizontally adjacent boxes labelled "Home" and "Global 
Common." Within the box labelled "Home", there is text reading "Data speed: 5 GB/s, Space 
per user: 40 GB." Within the box labelled "Global Common" there is text reading "Data 
speed: 100 GB/s, Space per user: N/A."
