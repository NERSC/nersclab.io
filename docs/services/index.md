# File Transfer Software at NERSC

- [bbcp](bbcp.md) - Bbcp is a point-to-point network file copy application with
excellent network transfer rates. This service is available on NERSC Data Transfer Nodes.

- [Globus](globus.md) - Recommended tool for moving large amounts of data between NERSC
and other systems.

- [GridFTP](gridftp.md) - Command Line interface for parallel data movement.

- [scp](scp.md) - Secure Copy (SCP) is used securely transfer files between two
hosts via SSH protocol.
 
- [Cern VM File System (CVMFS)](cvmfs.md) is a software distribution service to
provide data and software for jobs.

- [Frontier Cache](frontiercache.md) is a squid cache for distributing data efficiently 
to many clients around the world. Frontier caches are often used with CVMFS.

- [hsi and htar](../filesystems/archive.md) are tools for storing and managing data in 
the High-Performance Storage System (HPSS)

- [#give-and-take](../filesystems/sharing.md#sharing-one-time) are a set of companion 
utilities that allow a secure transfer of files from one user to another without 
exposing the files to third parties.
