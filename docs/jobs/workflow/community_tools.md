# Community Supported Tools

We encourage contributions from workflow tool developers 
and the NERSC user community on this page. NERSC staff may not be 
familiar with all workflow tools presented here, so we encourage 
you to reach out directly to the tool developers for questions and support.
To help us better understand which tools you are using or interested in, 
please don't hesitate to open a ticket at [help.nersc.gov](https://help.nersc.gov). 
This information is helpful for NERSC staff to gauge how best to support users.

## Papermill

[Papermill](papermill.md) is a tool that allows users to
run Jupyter notebooks 1) via the command line and 2) in an easily
parameterizable way. Papermill is best suited for Jupyter users who would
like to run the same notebook with different input values.

## Nextflow

[Nextflow](nextflow.md) is a data-centric workflow management tool
which facilitates complex and reproducible scientific computational workloads.

## libEnsemble

[libEnsemble](libensemble.md)
is a complete Python toolkit for steering dynamic ensembles of calculations.
Workflows are highly portable and easily detect/integrate heterogeneous resources.
For instance, libEnsemble can automatically detect, assign, and reassign allocated processors
and GPUs to ensemble members.
